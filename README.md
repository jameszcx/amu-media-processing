<p align="center">
    <img src="https://s1.ax1x.com/2023/07/25/pCO5pm6.png" width="100" loading="lazy">
</p>

<h1 align="center">Amu Media Processing</h1>


<h2 align="center">【请点击下方 :point_down: 分支链接，前往相应分支查看~ :smiling_imp: 】</h2>


## 项目介绍

视频的编码格式及封装格式纷繁复杂，而不同的播放器对格式的支持也不同，不同终端对视频流格式也有着特殊的需求；如只在前端对格式/大小进行简单的校验与拦截，是很难符合播放器播放要求的；在后端将上传的音视频转码压缩为统一的格式不仅可以大大降低服务器存储、带宽等压力，还可以大大提高播放端成功率；

基于FFfmpeg、Jave2、RabbitMQ、异步线程池、SQLite等实现了对音视频相关的处理，如视频转码、视频压缩、添加字幕、添加水印、视频截图等功能；

## 分支说明
- **independent** 独立版本分支 
  - 介绍：作为一个独立项目部署，通过调用对外提供的接口进行操作，适合并发量较大需要独立部署的场景；
  - 地址：:point_right: [独立版v1](https://gitee.com/pilaoyao/amu-media-processing/tree/independent-v1/)（开发中）

- **middleware** 中间件版本分支
  - 介绍：可以通过maven等方式嵌入自己的系统中，作为一个中间件来使用，使用起来会更加丝滑，比较适合对于流程有要求的场景；
  - 地址： :point_right: [中间件版v1](https://gitee.com/pilaoyao/amu-media-processing/tree/middleware-v1/)（待开发）


## 功能一览
[![pC6dfQx.png](https://s1.ax1x.com/2023/07/06/pC6dfQx.png)](https://imgse.com/i/pC6dfQx)

## 开发进度

[![pCcunpQ.png](https://s1.ax1x.com/2023/07/07/pCcunpQ.png)](https://imgse.com/i/pCcunpQ)


## 作者
<p align="left">
    <a href="#" target="_blank">
        <img src="https://pic.imgdb.cn/item/63f5822ff144a01007a5cd59.jpg" title="amu" width="10%">
    </a>
</p>
联系：cb1447003232


## 鸣谢



 感谢对开源项目的支持

## 支持

开源不易，如果觉得本项目还不错或在工作学习中有所启发，或者已经在使用了，麻烦前往 [Gitee]([Gitee](https://gitee.com/makunet/maku-cloud)) 点个 ⭐ Star 鼓励一下吧！